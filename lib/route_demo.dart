import 'package:flutter/material.dart';
import 'package:sw_core/request/request_auth_token.dart';
import 'package:sw_core/tool/log.dart';
import 'package:sw_core/interface/controller/i_controller_auth.dart';
import 'package:sw_demo/tool/test_data.dart';
import 'package:sw_core/entity_theme/theme_pair.dart';
import 'package:sw_loan_connect/init_loan_connect.dart';
import 'package:sw_loan_connect/routes/route_landing_page.dart';

class RouteDemo extends StatelessWidget {
  const RouteDemo({Key? key}) : super(key: key);

  @override
  build(BuildContext context) => Scaffold(
        appBar: _buildAppBar(),
        body: _buildBody(),
      );

  _buildAppBar() => AppBar(centerTitle: true, title: const Text('Spinwheel Dropin Modules Demo'));

  _buildBody() => SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 16, bottom: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: _routes.entries.map((entry) => _item(entry.value, entry.key)).toList(),
          ),
        ),
      );

  Widget _item(onPressed, text) => Padding(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(onPressed: onPressed, child: Text(text)),
      );
}

Map<String, Function> _routes = {
  'Loan Servicers Login': _onLoanServicersLogin,
  'LSL With Partner Provided Token': _onLoanServicersLoginWithPartnerProvidedToken,
  // 'Loan Pal': () => routeLoanPal(ParamsLoanPal()),
  // 'Loan List': () => routeLoanList(ParamsLoanList()),
  // 'Precision Pay': () => routePrecisionPay(ParamsPrecisionPay()),
  // 'Transaction History': () => routeTransactionHistory(ParamsTransactionHistory()),
};

_onLoanServicersLogin() => ThemePair.fromAsset('betterment_theme', 'dark_theme')
    .then(
      (theme) => initLoanConnect(
        auth: RequestAuthToken.pos(localHost, extUserID),
        theme: theme,
        events: _handleLoginEvents,
      ),
    )
    .whenComplete(() => routeLandingPage(ParamsLandingPage()));

_onLoanServicersLoginWithPartnerProvidedToken() => ThemePair.fromAsset('betterment_theme', 'dark_theme')
    .then(
      (theme) => initLoanConnect(
        token: 'REPLACE ME WITH A VALID TOKEN',
        theme: theme,
        events: _handleLoginEvents,
      ),
    )
    .whenComplete(() => routeLandingPage(ParamsLandingPage()));

_handleLoginEvents(EventHandlerLoanConnect handler) {
  handler.authStep.listen(_onAuthenticationChanged);
  handler.onPolicy = () => wtf("Privacy Policy Clicked: ${++handler.policyClicked.value}");
}

_onAuthenticationChanged(AuthStep authStep) {
  switch (authStep) {
    case AuthStep.STEP_UNAUTHORIZED:
    case AuthStep.STEP_SECURITY:
    case AuthStep.STEP_AUTHORIZED:
    case AuthStep.AUTH_ERROR:
      wtf(authStep);
  }
}
