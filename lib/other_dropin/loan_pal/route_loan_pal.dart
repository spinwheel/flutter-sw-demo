import 'dart:ui';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:sw_component/components/widget/card_button.dart';
import 'package:sw_component/components/widget/fetcher.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/sw_line_chart.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/entity_loan_calc/loan_calculated.dart';
import 'package:sw_core/request/request_loan_calc.dart';
import 'package:sw_core/tool/http_client.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/nav.dart';
import 'package:sw_data/data/endpoint_loan_calc.dart';
import 'package:sw_data/data/repo_loan_calc.dart';
import 'package:sw_data/usecase/use_case_loan_calculated.dart';
import 'package:sw_demo/other_dropin/bind_auth.dart';
import 'package:sw_loan_connect/routes/route_landing_page.dart';

routeLoanPal([ParamsLoanPal? params]) => navigate(
      () => RouteLoanPal(params: params ?? ParamsLoanPal()),
      binding: get<BindAuth>(),
    );

class ParamsLoanPal {
  String name;
  String transactionHistoryPath;
  String loanListNavigationText;
  String extraPayNavigationText;
  String roundUpNavigationText;
  String oneTimePayNavigationText;
  String collaboratorNavigationPath;
  String collaboratorNavigationText;

  bool hideTransactionHistoryNavigation;
  bool hideLoanListNavigation;
  bool hideExtraPayNavigation;
  bool hideRoundUpNavigation;
  bool showOneTimePayNavigation;
  bool hideCollaboratorNavigation;
  bool isNested;

  ParamsLoanPal({
    this.name = "",
    this.transactionHistoryPath = "",
    this.loanListNavigationText = "",
    this.extraPayNavigationText = "Extra Pay",
    this.roundUpNavigationText = "Round-Ups",
    this.oneTimePayNavigationText = "Make Payment",
    this.collaboratorNavigationPath = "",
    this.collaboratorNavigationText = "Community Pay",
    this.hideTransactionHistoryNavigation = false,
    this.hideLoanListNavigation = false,
    this.hideExtraPayNavigation = false,
    this.hideRoundUpNavigation = false,
    this.showOneTimePayNavigation = false,
    this.hideCollaboratorNavigation = false,
    this.isNested = true,
  });
}

class RouteLoanPal extends StatefulWidget {
  const RouteLoanPal({
    Key? key,
    required this.params,
    this.darkTheme,
    this.lightTheme,
  }) : super(key: key);

  final ParamsLoanPal params;
  final ThemeData? darkTheme;
  final ThemeData? lightTheme;

  @override
  createState() => _RouteLoanPalState();
}

class _RouteLoanPalState extends State<RouteLoanPal> {
  late LoanCalculated calc;

  @override
  Widget build(BuildContext context) => MaterialThemeWrapper(
        lightTheme: widget.lightTheme,
        darkTheme: widget.darkTheme,
        child: _buildBody(),
      );

  _buildBody() => Center(child: Fetcher(fetch: _fetchLoanCalc, onSuccess: _caseSuccess));

  Future<LoanCalculated> _fetchLoanCalc() =>
      UseCaseLoanCalc(RepoLoanCalc(EndpointLoanCalc(defaultHTTPClient()))).execute(
        RequestLoanCalc(
          loanBalance: 25000,
          interestRate: 5,
          monthlyPayment: 100,
          extraPayment: 20,
          isRecurring: true,
        ),
      );

  Widget _caseSuccess(LoanCalculated loanCalc) {
    calc = loanCalc;
    return SingleChildScrollView(child: _outerColumn());
  }

  _outerColumn() => Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(30),
            child: _innerColumn(),
          ),
        ],
      );

  _innerColumn() => Column(
        children: [
          Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4.0),
              shape: BoxShape.rectangle,
              border: Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
            ),
            child: Column(
              children: [
                ..._header(),
                ..._charts(),
              ],
            ),
          ),
          ..._cardButton(),
          ..._cards(),
        ],
      );

  _header() => [
        Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.only(left: 8, top: 8),
          child: const Text(
            'YOUR PROJECTIONS',
            style: TextStyle(fontSize: 12),
            textAlign: TextAlign.start,
          ),
        ),
        const SizedBox(height: 20),
        Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.only(left: 8),
          child: const Text(
            'Based on the information you provided, your current loan payoff will be in:',
            style: TextStyle(fontSize: 14),
            textAlign: TextAlign.start,
          ),
        ),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const ImageCompat(
              'calendar.svg',
              width: 72,
              height: 72,
            ),
            Column(
              children: [
                Text(
                  (calc.durationDebtRelief?.years ?? 0).toString(),
                  style: const TextStyle(
                    fontSize: 24,
                  ),
                ),
                const Text(
                  'Years',
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
            Column(
              children: [
                Text(
                  (calc.durationDebtRelief?.months ?? 0).toString(),
                  style: const TextStyle(
                    fontSize: 24,
                  ),
                ),
                const Text(
                  'Months',
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ],
            ),
            Column(
              children: [
                Text(
                  (calc.durationDebtRelief?.days ?? 0).toString(),
                  style: const TextStyle(
                    fontSize: 24,
                  ),
                ),
                const Text(
                  'Days',
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ],
        ),
      ];

  _charts() {
    final payAmount = calc.originalTotalPaymentAmount;
    final debtFree = calc.debtFree.toDouble();

    return [
      const SizedBox(height: 24),
      Padding(
        padding: const EdgeInsets.only(right: 20.0),
        child: SizedBox(
          height: 150,
          child: SWLineChart(
            y: payAmount,
            x: debtFree,
          ),
        ),
      ),
      const SizedBox(height: 24),
      SizedBox(
        height: 140,
        child: Stack(
          children: [
            PieChart(
              PieChartData(
                  borderData: FlBorderData(
                    show: false,
                  ),
                  sectionsSpace: 0,
                  centerSpaceRadius: 55,
                  startDegreeOffset: 90,
                  sections: showingSections()),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(currency(calc.originalTotalPaymentAmountRound, 0),
                      style: const TextStyle(fontSize: 19)),
                  const Text('Effective Debt', style: TextStyle(fontSize: 12)),
                ],
              ),
            ),
          ],
        ),
      ),
      const SizedBox(height: 32),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Principal', style: TextStyle(fontSize: 12)),
              Text(currency(calc.originalLoanBalance), style: const TextStyle(fontSize: 19)),
            ],
          ),
          Column(
            children: [
              const Text('Interest', style: TextStyle(fontSize: 12)),
              Text(currency(calc.originalTotalInterestAmount),
                  style: const TextStyle(fontSize: 19)),
            ],
          ),
        ],
      ),
    ];
  }

  _cardButton() => [
        const SizedBox(height: 16),
        CardButton(
          widget.params.roundUpNavigationText,
          'Put that spare change to work!',
          () => routeLandingPage(),
        ),
        const SizedBox(height: 16),
        CardButton(
          widget.params.extraPayNavigationText,
          'A few extra dollars each payment makes a BIG difference.',
          () => routeLandingPage(),
        ),
        const SizedBox(height: 16),
      ];

  _cards() => [
        Column(
          children: [
            Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                shape: BoxShape.rectangle,
                border: Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
              ),
              child: Column(
                children: [
                  const Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'YOUR STATUS',
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  ),
                  const SizedBox(height: 24),
                  SizedBox(
                    height: 140,
                    child: Stack(
                      children: [
                        PieChart(
                          PieChartData(
                              borderData: FlBorderData(
                                show: false,
                              ),
                              sectionsSpace: 0,
                              centerSpaceRadius: 55,
                              startDegreeOffset: 90,
                              sections: statusSlices()),
                        ),
                        Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(currency(25000), style: const TextStyle(fontSize: 19)),
                              const Text('Effective Debt', style: TextStyle(fontSize: 12)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 24),
                  const Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      'Click on loan from chart to see details',
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  ),
                  const SizedBox(height: 16),
                  const Divider(
                    color: Colors.black26,
                    height: 1,
                  ),
                  const SizedBox(height: 20),
                  Container(
                    alignment: AlignmentDirectional.center,
                    padding: const EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      shape: BoxShape.rectangle,
                      border:
                          Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
                    ),
                    child: TextButton(
                      onPressed: () => routeLandingPage(),
                      child: Text(
                        'Find out which loans to pay first',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            Container(
              alignment: AlignmentDirectional.topStart,
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                shape: BoxShape.rectangle,
                border: Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
              ),
              child: Column(
                children: [
                  const Text(
                    'YOUR PROGRESS',
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                  const SizedBox(height: 20),
                  const Text(
                    'Debt-Free',
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                  const SizedBox(height: 8),
                  const ImageCompat('status_bar.svg', width: 358, height: 8),
                  const SizedBox(height: 8),
                  Row(
                    children: [
                      const Expanded(
                        child: Text(
                          '0% Paid off',
                          style: TextStyle(
                            fontSize: 13,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Text(
                        currency(calc.originalTotalPaymentAmountRound, 0),
                        style: const TextStyle(
                          fontSize: 13,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  const Divider(
                    color: Colors.black26,
                    height: 1,
                  ),
                  const SizedBox(height: 20),
                  Container(
                    alignment: AlignmentDirectional.center,
                    padding: const EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      shape: BoxShape.rectangle,
                      border:
                          Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
                    ),
                    child: TextButton(
                      onPressed: () => navigate(
                          () => RouteLandingPage(params: ParamsLandingPage(isNested: false))),
                      child: Text(
                        'View Transactions',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ];

  List<PieChartSectionData> showingSections() {
    return List.generate(2, (i) {
      const radius = 20.0;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Theme.of(context).colorScheme.primary,
            title: "",
            value: calc.originalLoanBalance,
            radius: radius,
          );
        case 1:
          return PieChartSectionData(
            color: Theme.of(context).colorScheme.primaryVariant,
            title: "",
            value: calc.originalTotalInterestAmount,
            radius: radius,
          );
        default:
          throw Error();
      }
    });
  }

  List<PieChartSectionData> statusSlices() {
    return List.generate(4, (i) {
      const radius = 20.0;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Theme.of(context).colorScheme.primary,
            title: "",
            value: 4000,
            radius: radius,
          );
        case 1:
          return PieChartSectionData(
            color: Theme.of(context).colorScheme.primaryVariant,
            title: "",
            value: 4000,
            radius: radius,
          );
        case 2:
          return PieChartSectionData(
            color: Theme.of(context).colorScheme.secondary,
            title: "",
            value: 10000,
            radius: radius,
          );
        case 3:
          return PieChartSectionData(
            color: Theme.of(context).colorScheme.secondaryVariant,
            title: "",
            value: 7000,
            radius: radius,
          );
        default:
          throw Error();
      }
    });
  }
}
