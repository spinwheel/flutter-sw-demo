import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/box_card.dart';
import 'package:sw_component/components/widget/image_card.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/slim_appbar.dart';
import 'package:sw_component/components/widget/sw_line_chart.dart';
import 'package:sw_component/tool/ext_state.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/nav.dart';

import '../bind_auth.dart';

routePrecisionPay([ParamsPrecisionPay? params]) => navigate(
      () => RoutePrecisionPay(params: params ?? ParamsPrecisionPay()),
      binding: get<BindAuth>(),
    );

class ParamsPrecisionPay {
  String name;
  String transactionHistoryPath;
  String loanListNavigationText;
  String extraPayNavigationText;
  String roundUpNavigationText;
  String oneTimePayNavigationText;
  String collaboratorNavigationPath;
  String collaboratorNavigationText;

  bool hideTransactionHistoryNavigation;
  bool hideLoanListNavigation;
  bool hideExtraPayNavigation;
  bool hideRoundUpNavigation;
  bool showOneTimePayNavigation;
  bool hideCollaboratorNavigation;

  ParamsPrecisionPay({
    this.name = "",
    this.transactionHistoryPath = "",
    this.loanListNavigationText = "",
    this.extraPayNavigationText = "Extra Pay",
    this.roundUpNavigationText = "Round-Ups",
    this.oneTimePayNavigationText = "Make Payment",
    this.collaboratorNavigationPath = "",
    this.collaboratorNavigationText = "Community Pay",
    this.hideTransactionHistoryNavigation = false,
    this.hideLoanListNavigation = false,
    this.hideExtraPayNavigation = false,
    this.hideRoundUpNavigation = false,
    this.showOneTimePayNavigation = false,
    this.hideCollaboratorNavigation = false,
  });
}

class RoutePrecisionPay extends StatefulWidget {
  const RoutePrecisionPay({
    Key? key,
    required this.params,
    this.darkTheme,
    this.lightTheme,
  }) : super(key: key);

  final ParamsPrecisionPay params;
  final ThemeData? darkTheme;
  final ThemeData? lightTheme;

  @override
  State<RoutePrecisionPay> createState() => _RoutePrecisionPayState();
}

class _RoutePrecisionPayState extends State<RoutePrecisionPay> {
  int position = 0;
  double sliderValue = 20;
  double maxSliderValue = 500;
  String title = 'Extra Monthly Payment';

  List<Widget> get contentAmount => [
        Text(
          currency(sliderValue),
          style: const TextStyle(fontSize: 24),
        ),
        const Divider(),
      ];

  List<Widget> get contentMonth => [
        Text(
          '${sliderValue.round()} months',
          style: const TextStyle(fontSize: 24),
        ),
        const Divider(),
      ];
  late List<Widget> content = contentAmount;

  @override
  Widget build(BuildContext context) => MaterialThemeWrapper(
        lightTheme: widget.lightTheme,
        darkTheme: widget.darkTheme,
        child: _outerColumn(),
      );

  _outerColumn() => SingleChildScrollView(
        child: Column(
          children: [
            SlimAppBar('Make Payment'),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 30),
              child: _innerColumn(),
            ),
            _footer(),
          ],
        ),
      );

  _innerColumn() => Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _toggleCard(),
          const SizedBox(height: 16),
          Text('Loan Impact', style: TextStyle(fontSize: 20, color: colorScheme.onBackground)),
          Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4.0),
              shape: BoxShape.rectangle,
              border: Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Total Amount Pay',
                    style: TextStyle(fontSize: 14, color: colorScheme.onBackground)),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Column(
                      children: [
                        Text('Current',
                            style: TextStyle(fontSize: 14, color: colorScheme.onBackground)),
                        const SizedBox(height: 8),
                      ],
                    ),
                    const SizedBox(width: 24),
                    Column(
                      children: [
                        Text('W/Spinwheel',
                            style: TextStyle(fontSize: 14, color: colorScheme.onBackground)),
                        const SizedBox(height: 8),
                      ],
                    ),
                    const SizedBox(width: 24),
                    Column(
                      children: [
                        Text('Savings',
                            style: TextStyle(fontSize: 14, color: colorScheme.onBackground)),
                        const SizedBox(height: 8),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(height: 24),
          Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4.0),
              shape: BoxShape.rectangle,
              border: Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Total Time Remaining',
                    style: TextStyle(fontSize: 14, color: colorScheme.onBackground)),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Column(
                      children: [
                        Text('Current',
                            style: TextStyle(fontSize: 14, color: colorScheme.onBackground)),
                        const SizedBox(height: 8),
                      ],
                    ),
                    const SizedBox(width: 24),
                    Column(
                      children: [
                        Text('W/Spinwheel',
                            style: TextStyle(fontSize: 14, color: colorScheme.onBackground)),
                        const SizedBox(height: 8),
                      ],
                    ),
                    const SizedBox(width: 24),
                    Column(
                      children: [
                        Text('Savings',
                            style: TextStyle(fontSize: 14, color: colorScheme.onBackground)),
                        const SizedBox(height: 8),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(height: 24),
          const BoxCard(
            child: SWLineChart(
              y: 60000,
              x: 15,
            ),
          ),
          const SizedBox(height: 24),
          const ImageCard.pos(
              'flowered_window.png',
              'CONNECT YOUR BANK ACCOUNT',
              'Required to make payments',
              'Get out of debt faster and save money by connecting your bank account'),
        ],
      );

  _toggleCard() => Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.0),
          shape: BoxShape.rectangle,
          border: Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(fontSize: 14, color: colorScheme.onBackground),
            ),
            const SizedBox(height: 24),
            Row(
              children: [
                // ToggleSwitch(
                //   totalSwitches: 2,
                //   onToggle: (int position) {
                //     bool samePosition = this.position == position;
                //     if (!samePosition) {
                //       this.position = position;
                //       sliderValue = _calcNewSliderValue(position);
                //     }
                //     setState(() {
                //       title = _changeTitle(position);
                //       content = _changeContent(position);
                //     });
                //   },
                //   initialLabelIndex: 0,
                //   labels: const ['Amount', 'Month'],
                //   activeFgColor: colorScheme.onSurface,
                //   inactiveBgColor: colorScheme.surface,
                // ),
                const SizedBox(width: 16),
                Column(mainAxisAlignment: MainAxisAlignment.start, children: content),
              ],
            ),
            const SizedBox(height: 24),
            Text(
              'Range',
              style: TextStyle(fontSize: 14, color: colorScheme.onBackground),
            ),
            const SizedBox(height: 24),
            Slider(
              value: sliderValue,
              min: 0.0,
              max: maxSliderValue,
              onChanged: (double newValue) {
                setState(() {
                  sliderValue = newValue;
                  content = _changeContent(position);
                });
              },
              inactiveColor: colorScheme.surface,
              activeColor: colorScheme.onSurface,
            ),
          ],
        ),
      );

  _changeTitle(int position) {
    switch (position) {
      case 1:
        return 'Goal Months Early';
      case 0:
      default:
        return 'Extra Monthly Payment';
    }
  }

  _changeContent(int position) {
    switch (position) {
      case 1:
        return contentMonth;
      case 0:
      default:
        return contentAmount;
    }
  }

  _calcNewSliderValue(int position) {
    const double maxAmount = 500;
    const double maxMonth = 184;
    const bool amountIsBigger = maxAmount > maxMonth;
    const double factor = maxMonth / maxAmount;

    switch (position) {
      case 1:
        maxSliderValue = maxMonth;
        return amountIsBigger ? sliderValue * factor : sliderValue / factor;
      case 0:
      default:
        maxSliderValue = maxAmount;
        return amountIsBigger ? sliderValue / factor : sliderValue * factor;
    }
  }

  _footer() => SizedBox(
        height: 64,
        width: double.infinity,
        child: ElevatedButton(
          onPressed: () {
            null;
          },
          child: Text(
            'PAY NOW',
            style: TextStyle(
              color: colorScheme.onPrimary,
              backgroundColor: colorScheme.primary,
            ),
          ),
        ),
      );
}
