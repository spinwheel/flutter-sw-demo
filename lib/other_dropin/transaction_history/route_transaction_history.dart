import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/fetcher.dart';
import 'package:sw_component/components/widget/history_card.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_component/components/widget/loader.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/slim_sliver_appbar.dart';
import 'package:sw_component/tool/ext_state.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/entity_loan_transaction/loan_transaction.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/response/response_transactions.dart';
import 'package:sw_core/tool/http_client.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/nav.dart';
import 'package:sw_data/data/endpoint_servicer.dart';
import 'package:sw_data/data/endpoint_transactions.dart';
import 'package:sw_data/data/repo_servicer.dart';
import 'package:sw_data/data/repo_transaction.dart';
import 'package:sw_data/usecase/use_case_servicer.dart';
import 'package:sw_data/usecase/use_case_transactions.dart';
import '../bind_auth.dart';

routeTransactionHistory([ParamsTransactionHistory? params]) => navigate(
      () => RouteTransactionHistory(params: params ?? ParamsTransactionHistory()),
      binding: get<BindAuth>(),
    );

class ParamsTransactionHistory {
  String name;
  String transactionHistoryPath;
  String loanListNavigationText;
  String extraPayNavigationText;
  String roundUpNavigationText;
  String oneTimePayNavigationText;
  String collaboratorNavigationPath;
  String collaboratorNavigationText;

  bool hideTransactionHistoryNavigation;
  bool hideLoanListNavigation;
  bool hideExtraPayNavigation;
  bool hideRoundUpNavigation;
  bool showOneTimePayNavigation;
  bool hideCollaboratorNavigation;
  bool isNested;

  ParamsTransactionHistory({
    this.name = "",
    this.transactionHistoryPath = "",
    this.loanListNavigationText = "",
    this.extraPayNavigationText = "Extra Pay",
    this.roundUpNavigationText = "Round-Ups",
    this.oneTimePayNavigationText = "Make Payment",
    this.collaboratorNavigationPath = "",
    this.collaboratorNavigationText = "Community Pay",
    this.hideTransactionHistoryNavigation = false,
    this.hideLoanListNavigation = false,
    this.hideExtraPayNavigation = false,
    this.hideRoundUpNavigation = false,
    this.showOneTimePayNavigation = false,
    this.hideCollaboratorNavigation = false,
    this.isNested = true,
  });
}

class RouteTransactionHistory extends StatefulWidget {
  const RouteTransactionHistory({
    Key? key,
    required this.params,
    this.darkTheme,
    this.lightTheme,
  }) : super(key: key);

  final ParamsTransactionHistory params;
  final ThemeData? darkTheme;
  final ThemeData? lightTheme;

  @override
  State<RouteTransactionHistory> createState() => _RouteTransactionHistoryState();
}

class _RouteTransactionHistoryState extends State<RouteTransactionHistory> {
  late List<LoanTransaction> loanTransactions;
  String? _chosenValue;

  @override
  Widget build(BuildContext context) => MaterialThemeWrapper(
        lightTheme: widget.lightTheme,
        darkTheme: widget.darkTheme,
        child: _body(),
      );

  _body() => SafeArea(
        child: Center(
          child: Fetcher(fetch: _fetchTransactions, onSuccess: _caseSuccess),
        ),
      );

  Future<ResponseTransactions> _fetchTransactions() =>
      UseCaseTransactions(RepoTransactions(EndpointTransactions(defaultHTTPClient()))).execute();

  Widget _caseSuccess(ResponseTransactions response) {
    loanTransactions = response.loanTransactions ?? [];
    return widget.params.isNested ? _scrollNested() : _scroll();
  }

  _scroll() => CustomScrollView(
        slivers: [
          const SlimSliverAppBar('Payments'),
          SliverList(delegate: SliverChildListDelegate(_content())),
        ],
      );

  _scrollNested() => SingleChildScrollView(
        child: Column(
          children: _content(),
        ),
      );

  List<Widget> _content() => [
        _filter(),
        ...List.generate(
          loanTransactions.length,
          (index) {
            LoanTransaction item = loanTransactions[index];
            return Fetcher(
              fetch: () => UseCaseServicer(RepoServicer(EndpointServicer(defaultHTTPClient())))
                  .execute(item.loanServicerId),
              onLoading: () => const HistoryCard(image: Loader(width: 24, height: 24)),
              onSuccess: (Servicer servicer) => HistoryCard(
                image: ImageCompat(
                  servicer.logoUrl,
                  width: 24,
                  height: 24,
                ),
                index: (index + 1).toString(),
                date: formatUSA(item.paymentDate),
                value: currency(item.paymentAmount),
                detail0: item.status,
                detail1: item.paymentType,
              ),
            );
          },
        ),
      ];

  _filter() => Padding(
        padding: const EdgeInsets.fromLTRB(0, 16, 30, 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Filter by',
                    textAlign: TextAlign.start,
                  ),
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: DropdownButton<String>(
                    dropdownColor: colorScheme.primary,
                    focusColor: colorScheme.primary,
                    value: _chosenValue,
                    elevation: 5,
                    style: TextStyle(color: colorScheme.onPrimary),
                    iconEnabledColor: colorScheme.primary,
                    items: <String>[
                      'View All Transactions',
                      'Mine Only',
                      'MOCK Only',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value,
                          style: const TextStyle(color: Colors.white),
                        ),
                      );
                    }).toList(),
                    hint: const Text(
                      "View All Transactions",
                      style: TextStyle(color: Colors.white),
                    ),
                    onChanged: (String? value) {
                      setState(() => _chosenValue = value);
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      );
}
