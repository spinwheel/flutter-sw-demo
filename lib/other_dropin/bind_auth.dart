import 'package:sw_auth/sw_init_auth.dart';
import 'package:sw_core/request/request_auth_token.dart';
import 'package:sw_core/tool/base_bindings.dart';
import 'package:sw_data/inject_spinwheel_api_wrapper.dart';

class BindAuth extends BaseBindings {
  BindAuth({this.requestAuthToken, this.token});

  RequestAuthToken? requestAuthToken;
  String? token;

  @override
  void init() {
    initAuth(requestToken: requestAuthToken, token: token);
    initData();
  }
}