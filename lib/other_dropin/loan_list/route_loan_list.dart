import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_component/components/widget/card_button.dart';
import 'package:sw_component/components/widget/data_card.dart';
import 'package:sw_component/components/widget/fetcher.dart';
import 'package:sw_component/components/widget/material_theme_wrapper.dart';
import 'package:sw_component/components/widget/sliver_scroll.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/entity_user/user.dart';
import 'package:sw_core/interface/controller/i_controller_all_servicers.dart';
import 'package:sw_core/interface/controller/i_controller_auth.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/nav.dart';
import 'package:sw_core/tool/observer.dart';
import 'package:sw_demo/other_dropin/loan_list/route_loan_details.dart';

import '../bind_auth.dart';

routeLoanList([ParamsLoanList? params]) => navigate(
      () => RouteLoanList(params: params ?? ParamsLoanList()),
      binding: get<BindAuth>(),
    );

class ParamsLoanList {
  ParamsLoanList({
    this.isNested = true,
  });

  bool isNested;
}

class RouteLoanList extends StatelessWidget {
  RouteLoanList({
    required this.params,
    this.darkTheme,
    this.lightTheme,
    Key? key,
  }) : super(key: key);

  final ParamsLoanList params;
  final ThemeData? darkTheme;
  final ThemeData? lightTheme;

  final IControllerAllServicers controllerServicer = get<IControllerAllServicers>();
  final IControllerAuth controllerAuth = get<IControllerAuth>();

  User get user => controllerAuth.user.value;

  @override
  Widget build(BuildContext context) => MaterialThemeWrapper(
        lightTheme: lightTheme,
        darkTheme: darkTheme,
        child: _body(),
      );

  _body() => Center(child: _caseUserSuccess());

  Widget _caseUserSuccess() {
    return params.isNested ? _nestedContent() : _content();
  }

  _content() => SliverScroll(
        title: 'Loans',
        children: [
          _scrollable(),
        ],
      );

  _nestedContent() => Center(
        child: SingleChildScrollView(child: _scrollable()),
      );

  _scrollable() => Padding(
        padding: const EdgeInsets.fromLTRB(30, 0, 30, 30),
        child: Observer(
          () => Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ..._loans(),
              const SizedBox(height: 32),
              ..._footerButtons(),
            ],
          ),
        ),
      );

  List<Widget> _loans() {
    List<Widget> list = [];

    for (var sla in user.studentLoanAccounts) {
      Servicer? servicer;
      String servicerID = sla.loanServicerId ?? "";
      if (servicerID.isNotEmpty) {
        list.add(Fetcher<Servicer>(
          fetch: () => controllerServicer.getServicer(servicerID),
          onSuccess: (serv) {
            servicer = serv;
            return Column(
              children: [
                const SizedBox(height: 16),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    servicer?.name ?? "",
                    style: const TextStyle(fontSize: 26),
                  ),
                ),
                const SizedBox(height: 16),
              ],
            );
          },
        ));
      }

      for (var acc in sla.loanAccounts) {
        for (var loan in acc.loans!) {
          list.add(
            DataCard(
              currency(loan.origAmount, 0),
              formatUTC(loan.dueDate ?? ""),
              loan.interestRate.toString(),
              loan.status ?? "",
              () {
                navigate(
                  () => const RouteLoanDetails(),
                  arguments: {
                    'loan': loan,
                    'servicer': servicer,
                  },
                );
              },
            ),
          );
        }
      }
    }

    return list;
  }

  _footerButtons() => const [
        CardButton(
          'Spare Change Round Up',
          'Save 2 years & 5k* - Put that spare change to work',
        ),
        CardButton(
          'Extra Pay',
          'Save 4 years & 10k* - A few extra dollars each payment makes a BIG difference',
        )
      ];
}
