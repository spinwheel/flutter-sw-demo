import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:sw_component/components/widget/image_compat.dart';
import 'package:sw_component/components/widget/slim_appbar.dart';
import 'package:sw_component/tool/format.dart';
import 'package:sw_core/entity_servicer/servicer.dart';
import 'package:sw_core/entity_user/loans.dart';
import 'package:sw_core/tool/nav.dart';
import 'package:sw_core/tool/state.dart';

class RouteLoanDetails extends StatefulWidget {
  const RouteLoanDetails({Key? key}) : super(key: key);

  @override
  State<RouteLoanDetails> createState() => _RouteLoanDetailsState();
}

class _RouteLoanDetailsState extends State<RouteLoanDetails> {
  late Loans loan;
  late Servicer servicer;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: SlimAppBar('Loan Info'),
        body: _buildBody(),
      );

  _buildBody() {
    loan = args['loan'];
    servicer = args['servicer'];
    return Column(
      children: [
        _innerColumn(),
        _footer(),
      ],
    );
  }

  _innerColumn() => Expanded(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 30),
            child: Column(
              children: [
                _logo(),
                _details(),
                _percentageCard(),
              ],
            ),
          ),
        ),
      );

  _logo() => Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(32),
        child: ImageCompat(
          servicer.logoUrl,
          width: 192,
          height: 192,
        ),
      );

  _details() => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const SizedBox(height: 8),
          Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Interest Rate',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    '${loan.interestRate.toString()}%',
                    style: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(height: 32),
                  const Text(
                    'Status',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    loan.status ?? "",
                    style: const TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 32),
                  const Text(
                    'Principal Balance',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    currency(loan.principalBalance),
                    style: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(height: 32),
                  const Text(
                    'Loan Type',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    loan.loanType ?? "",
                    style: const TextStyle(fontSize: 16),
                  ),
                ],
              ),
              const SizedBox(width: 32),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Original Amount',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    currency(loan.origAmount),
                    style: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(height: 32),
                  const Text(
                    'Minimum Payment',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    currency(loan.regularMonthlyPayment),
                    style: const TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 32),
                  const Text(
                    'Outstanding Interest',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    loan.outstandingInterest.toString(),
                    style: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(height: 32),
                  const Text(
                    'Repayment Plan',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    loan.repaymentPlan.toString(),
                    style: const TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(height: 32),
        ],
      );

  _percentageCard() => Container(
        alignment: AlignmentDirectional.topStart,
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.0),
          shape: BoxShape.rectangle,
          border: Border.all(color: Colors.black12, width: 1.0, style: BorderStyle.solid),
          color: colorScheme.surface,
          boxShadow: [
            BoxShadow(
              color: isDark ? Colors.transparent : Colors.grey.withOpacity(0.2),
              spreadRadius: 3,
              blurRadius: 4,
              offset: const Offset(-2, 1), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              children: const [
                Text(
                  'Debt-Free',
                  style: TextStyle(fontSize: 14),
                ),
                SizedBox(width: 62),
                Text(
                  'Estimated',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '',
                  style: TextStyle(fontSize: 14),
                ),
              ],
            ),
            const SizedBox(height: 24),
            LinearPercentIndicator(
              curve: Curves.easeInOut,
              animation: true,
              animationDuration: 2000,
              lineHeight: 8.0,
              percent: _calcPaidOff(),
              progressColor: colorScheme.primary,
            ),
            const SizedBox(height: 24),
            Text('${(_calcPaidOff() * 100).toStringAsFixed(2)}% Paid-Off',
                style: const TextStyle(fontSize: 12)),
          ],
        ),
      );

  _footer() => SizedBox(
        height: 64,
        width: double.infinity,
        child: ElevatedButton(
          onPressed: () {},
          child: const Text(
            'Configure Payment',
            style: TextStyle(fontSize: 16),
          ),
        ),
      );

  _calcPaidOff() {
    double toPayPercent = (loan.principalBalance!.toDouble() / loan.origAmount!.toDouble());
    return 1 - toPayPercent;
  }
}
