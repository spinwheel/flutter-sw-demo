import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sw_core/request/request_auth_token.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/interface/controller/i_controller_auth.dart';
import 'package:sw_core/tool/log.dart';
import 'package:sw_core/entity_theme/theme_pair.dart';
import 'package:sw_demo/tool/test_data.dart';
import 'package:sw_loan_connect/bind_loan_connect.dart';
import 'package:sw_loan_connect/init_loan_connect.dart';
import 'package:sw_loan_connect/routes/route_landing_page.dart';
import 'package:sw_component/service/service_theme.dart';

void main() {
  _loadTheme().then(_init).whenComplete(_runApp);
}

_loadTheme() => ThemePair.fromAsset('betterment_theme', 'dark_theme');

_init(ThemePair theme) => initLoanConnect(
  auth: RequestAuthToken.pos(localHost, extUserID),
  theme: theme,
  events: _handleEvents,
);

_runApp() => runApp(const LoanConnectDemo());

class LoanConnectDemo extends GetMaterialApp {
  const LoanConnectDemo({Key? key}) : super(key: key);

  @override
  Bindings? get initialBinding => get<BindLoanConnect>();

  @override
  Widget get home => RouteLandingPage(params: ParamsLandingPage(isNested: false));

  @override
  String get title => 'Loan Connect Demo';

  @override
  bool get debugShowCheckedModeBanner => false;

  @override
  ThemeMode get themeMode => ThemeMode.system;

  @override
  ThemeData get theme => get<ServiceTheme>().theme;

  @override
  ThemeData get darkTheme => get<ServiceTheme>().darkTheme;
}

_handleEvents(EventHandlerLoanConnect handler) {
  handler.authStep.listen(_onAuthenticationChanged);
  handler.onPolicy = () => wtf("Privacy Policy Clicked: ${++handler.policyClicked.value}");
}

_onAuthenticationChanged(AuthStep authStep) {
  switch (authStep) {
    case AuthStep.STEP_UNAUTHORIZED:
    case AuthStep.STEP_SECURITY:
    case AuthStep.STEP_AUTHORIZED:
    case AuthStep.AUTH_ERROR:
      wtf(authStep);
  }
}
