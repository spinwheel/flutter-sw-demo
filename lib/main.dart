import 'package:flutter/material.dart';
import 'package:sw_component/tool/sample_theme.dart';
import 'package:get/get.dart';
import 'package:sw_demo/route_demo.dart';

void main() => runApp(const SwDemoApp());

class SwDemoApp extends GetMaterialApp {
  const SwDemoApp({Key? key}) : super(key: key);

  @override
  Widget get home => const RouteDemo();

  @override
  String get title => 'Spinwheel Dropin Demo';

  @override
  bool get debugShowCheckedModeBanner => false;

  @override
  ThemeMode get themeMode => ThemeMode.system;

  @override
  ThemeData get theme => SampleTheme.lightTheme;

  @override
  ThemeData get darkTheme => SampleTheme.darkTheme;
}
