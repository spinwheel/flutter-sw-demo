# Release Notes

## _Functionality_

Current release version fully supports user authentication for integrated loan servicers.

Login events can be observed and reacted upon, notifying observers for whenever user attempted, failed or succeded at login.

![eventhandler.png](https://www.dropbox.com/s/kp2e9jxazkc0c8j/eventhandler.png?dl=0&raw=1)

Usage is fully demonstrated on this repository at either run_loan_connect.dart, which covers exclusively the connect module, or main.dart for emulation of a consumer-app which then will access connect module.

Whatever Theme a partner has setup and already running on its consumer-app, will be adopted by our widgets as is natural with Flutter's theme inheritance. Should the partner opt to override this we currently support theming through a .json asset. This last is fully functional for Desktop, with mobile and web underway.

![loanconnect.gif](https://www.dropbox.com/s/wadlcsfbw8gqqsc/loanconnect.gif?dl=0&raw=1)

## _Known Issues_

Earnest's login still requires additional implementation.
Some Privacy Policy and Forgot Password links may require updating.
Not all SVG formats are supported. We have a workaround for current assets.
Theming with .json files for mobile and web are underway.
Partner's theme properties may not match currently mapped fields on our widgets.