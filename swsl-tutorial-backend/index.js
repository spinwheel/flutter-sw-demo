require('dotenv').config();
const { default: axios } = require('axios');
const express = require('express');
const cors = require('cors');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cors());
app.options('*', cors());

const getToken = async (extUserId, userId, tokenExpiryInSec) => {
  try {
    const params = extUserId ? { extUserId, tokenExpiryInSec } : { userId, tokenExpiryInSec };
  
    if (process.env.SECRET_KEY === 'f3322d59-7a55-4144-abf7-30e7dcfa2283') {
      console.log('You have not changed the secretKey. Since this is a dummy value, this fetch will fail. Please get a set of credentials and replace the secretKey');
    }
    const resp = await axios({
      method: 'post',
      url: process.env.BASE_URL,
      data: params,
      headers: {
        Authorization: `Bearer ${process.env.SECRET_KEY}`,
      },
    });
    return resp.data.data;
  } catch (error) {
    throw error; 
  }
};

app.post('/',async (req, res) => {
  const { extUserId, userId, tokenExpiryInSec} = req.body;
  try {
    const data = await getToken(extUserId, userId, tokenExpiryInSec)
    res.send({
      "status": {
        "code": 200,
        "desc": "success"
      },
      "data": data
    });  
  } catch(e) {
      res.status(500).send({
        "status": {
          "code": 500,
          "desc": e.message
        },
      });
   };
});

app.listen(process.env.PORT, () =>
  console.log(`Server started on port ${process.env.PORT}`)
);
